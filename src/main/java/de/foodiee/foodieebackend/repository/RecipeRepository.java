package de.foodiee.foodieebackend.repository;

import java.util.Collection;

import org.springframework.data.repository.CrudRepository;

import de.foodiee.foodieebackend.entity.Recipe;

public interface RecipeRepository extends CrudRepository<Recipe, Long> {

    Collection<Recipe> findRecipesByOrderByIdDesc();

    Recipe findRecipeById(Long id);
}
