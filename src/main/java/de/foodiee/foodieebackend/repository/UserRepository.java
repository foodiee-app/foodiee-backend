package de.foodiee.foodieebackend.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import de.foodiee.foodieebackend.entity.User;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

    User findUserById(long id);
}
