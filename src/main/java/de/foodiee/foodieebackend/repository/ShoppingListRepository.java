package de.foodiee.foodieebackend.repository;

import org.springframework.data.repository.CrudRepository;

import de.foodiee.foodieebackend.entity.ShoppingListEntry;

public interface ShoppingListRepository extends CrudRepository<ShoppingListEntry, Long> {
    ShoppingListEntry findByIngredientId(Long ingredientId);
}
