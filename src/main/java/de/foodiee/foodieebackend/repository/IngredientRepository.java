package de.foodiee.foodieebackend.repository;

import org.springframework.data.repository.CrudRepository;

import de.foodiee.foodieebackend.entity.Ingredient;

public interface IngredientRepository extends CrudRepository<Ingredient, Long> {
    Ingredient findByNameIgnoreCase(String name);
}
