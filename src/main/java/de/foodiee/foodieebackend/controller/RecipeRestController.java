package de.foodiee.foodieebackend.controller;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import de.foodiee.foodieebackend.controller.requestbody.RecipeRequestBody;
import de.foodiee.foodieebackend.controller.requestbody.RecipeRequestBody.RecipeIngredientElement;
import de.foodiee.foodieebackend.entity.Ingredient;
import de.foodiee.foodieebackend.entity.Recipe;
import de.foodiee.foodieebackend.repository.IngredientRepository;
import de.foodiee.foodieebackend.repository.RecipeRepository;
import de.foodiee.foodieebackend.utility.HashUtils;
import de.foodiee.foodieebackend.utility.Utility;
import lombok.extern.log4j.Log4j2;

@CrossOrigin
@RestController
@Log4j2
public class RecipeRestController {

    private final RecipeRepository recipeRepository;
    private final IngredientRepository ingredientRepository;
    private final String foodieeBackendUrl;

    @Autowired
    public RecipeRestController(RecipeRepository recipeRepository,
            IngredientRepository ingredientRepository,
            @Value("${foodiee.backend.url}") String foodieeBackendUrl) {
        this.recipeRepository = recipeRepository;
        this.ingredientRepository = ingredientRepository;
        this.foodieeBackendUrl = foodieeBackendUrl;
    }


    @GetMapping(path = "/recipes")
    public Collection<RecipeRequestBody> fetchAllRecipes() {
        Collection<Recipe> recipes = recipeRepository.findRecipesByOrderByIdDesc();
        Collection<RecipeRequestBody> recipesJson = new ArrayList<>();
        for (Recipe recipe : recipes) {
            log.info(recipe.getName());
            recipe.setPreparation(recipe.getPreparation().replace("\\n", "\n"));
            recipesJson.add(new RecipeRequestBody(recipe));
        }
        return recipesJson;
    }

    @GetMapping(path = "/recipes/{id}")
    public RecipeRequestBody fetchRecipeById(@PathVariable Long id) {
        Recipe recipe = recipeRepository.findRecipeById(id);
        recipe.setPreparation(recipe.getPreparation().replace("\\n", "\n"));
        return new RecipeRequestBody(recipe);
    }

    @PostMapping(value = "/recipes/upload")
    public String uploadFile(@RequestParam MultipartFile file) {
        if (file.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }

        Objects.requireNonNull(file.getOriginalFilename());
        String fileUploadPath;
        try {
            fileUploadPath = "images/" + HashUtils.getMd5(file.getBytes()) + "." +
                    Utility.getFileType(file.getOriginalFilename());
        } catch (IOException exception) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        try (BufferedOutputStream outputStream = new BufferedOutputStream(
                new FileOutputStream("public/" + fileUploadPath))) {
            byte[] bytes = file.getBytes();
            outputStream.write(bytes);
        } catch (IOException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }

        return foodieeBackendUrl + fileUploadPath;
    }

    @PostMapping(path = "/recipes/create")
    public RecipeRequestBody createRecipe(@RequestBody RecipeRequestBody requestBody) {

        Recipe recipe = new Recipe(requestBody);
        for (RecipeIngredientElement ingredient : requestBody.getIngredients()) {
            Ingredient existingIngredient = ingredientRepository.findByNameIgnoreCase(ingredient.getName());
            if (existingIngredient != null) {
                recipe.addIngredient(existingIngredient, ingredient.getAmount());
                continue;
            }

            recipe.addIngredient(ingredientRepository.save(new Ingredient(ingredient.getName(), ingredient.getUnit())),
                    ingredient.getAmount());
        }

        return new RecipeRequestBody(recipeRepository.save(recipe));
    }
}
