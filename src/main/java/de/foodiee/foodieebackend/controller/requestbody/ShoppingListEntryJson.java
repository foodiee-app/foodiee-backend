package de.foodiee.foodieebackend.controller.requestbody;

import de.foodiee.foodieebackend.entity.Ingredient;
import de.foodiee.foodieebackend.entity.ShoppingListEntry;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ShoppingListEntryJson {

    private long ingredientId;
    private int totalAmount;
    private String name;
    private String unit;

    protected ShoppingListEntryJson() {
    }

    public ShoppingListEntryJson(ShoppingListEntry listEntry) {
        Ingredient ingredient = listEntry.getIngredient();

        ingredientId = listEntry.getIngredientId();
        totalAmount = listEntry.getTotalAmount();
        name = ingredient.getName();
        unit = ingredient.getUnit();
    }
}
