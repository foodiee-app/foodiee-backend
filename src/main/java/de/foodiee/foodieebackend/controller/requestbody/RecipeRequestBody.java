package de.foodiee.foodieebackend.controller.requestbody;

import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonProperty;

import de.foodiee.foodieebackend.entity.Ingredient;
import de.foodiee.foodieebackend.entity.Recipe;
import de.foodiee.foodieebackend.entity.RecipeIngredient;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RecipeRequestBody {

    private long id;
    private long userId;
    private String name;
    private String thumbnailPath;
    private boolean isVeggie;
    private int preparationTime;
    private String preparation;
    private final Set<RecipeIngredientElement> ingredients = new HashSet<>();

    protected RecipeRequestBody() {
    }

    public RecipeRequestBody(Recipe recipe) {
        id = recipe.getId();
        userId = recipe.getUserId();
        name = recipe.getName();
        thumbnailPath = recipe.getThumbnailPath();
        isVeggie = recipe.isVeggie();
        preparationTime = recipe.getPreparationTime();
        preparation = recipe.getPreparation();
        for(RecipeIngredient recipeIngredient : recipe.getRecipeIngredients()) {
            ingredients.add(new RecipeIngredientElement(recipeIngredient));
        }
    }

    @JsonProperty("isVeggie")
    public boolean isVeggie() {
        return isVeggie;
    }

    @JsonProperty("isVeggie")
    public void setVeggie(boolean veggie) {
        isVeggie = veggie;
    }

    @Getter
    public static class RecipeIngredientElement {

        private long id;
        private String name;
        private String unit;
        private int amount;

        protected RecipeIngredientElement() {
        }

        public RecipeIngredientElement(RecipeIngredient recipeIngredient) {
            Ingredient ingredient = recipeIngredient.getIngredient();

            id = ingredient.getId();
            name = ingredient.getName();
            unit = ingredient.getUnit();
            amount = recipeIngredient.getAmount();
        }
    }
}
