package de.foodiee.foodieebackend.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import de.foodiee.foodieebackend.entity.User;
import de.foodiee.foodieebackend.repository.UserRepository;

@CrossOrigin
@RestController
public class UserRestController {

    private final UserRepository userRepository;

    @Autowired
    public UserRestController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    @GetMapping(path = "/users")
    public Collection<User> fetchAllUsers() {
        return (Collection<User>) userRepository.findAll();
    }

    @GetMapping(path = "/users/{id}")
    public User fetchUserById(@PathVariable Long id) {
        return userRepository.findUserById(id);
    }
}
