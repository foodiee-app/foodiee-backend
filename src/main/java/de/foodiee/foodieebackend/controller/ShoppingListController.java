package de.foodiee.foodieebackend.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import de.foodiee.foodieebackend.controller.requestbody.RecipeRequestBody;
import de.foodiee.foodieebackend.controller.requestbody.RecipeRequestBody.RecipeIngredientElement;
import de.foodiee.foodieebackend.controller.requestbody.ShoppingListEntryJson;
import de.foodiee.foodieebackend.entity.ShoppingListEntry;
import de.foodiee.foodieebackend.repository.ShoppingListRepository;

@CrossOrigin
@RestController
public class ShoppingListController {

    private final ShoppingListRepository shoppingListRepository;

    @Autowired
    public ShoppingListController(ShoppingListRepository shoppingListRepository) {
        this.shoppingListRepository = shoppingListRepository;
    }


    @PostMapping(path = "/shopping-list/clear")
    public void clearList() {
        shoppingListRepository.deleteAll();
    }

    @PostMapping(path = "/shopping-list")
    public void addShoppingList(@RequestBody RecipeRequestBody requestBody) {
        for(RecipeIngredientElement element : requestBody.getIngredients()) {
            ShoppingListEntry shoppingListEntry = shoppingListRepository.findByIngredientId(element.getId());

            if(shoppingListEntry == null) {
                shoppingListEntry = new ShoppingListEntry(element.getId(), element.getAmount());
            } else {
                shoppingListEntry.setTotalAmount(shoppingListEntry.getTotalAmount() + element.getAmount());
            }

            shoppingListRepository.save(shoppingListEntry);
        }
    }

    @GetMapping(path = "/shopping-list")
    public Collection<ShoppingListEntryJson> fetchShoppingList() {
        List<ShoppingListEntryJson> entries = new ArrayList<>();

        for(ShoppingListEntry listEntry : shoppingListRepository.findAll()) {
            entries.add(new ShoppingListEntryJson(listEntry));
        }
        return entries;
    }
}
