package de.foodiee.foodieebackend.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;

@Entity
@Table(name = "ingredient")
@Getter
public class Ingredient {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ingredient_id")
    private long id;
    private String name;
    private String unit;

    protected Ingredient() {
    }

    public Ingredient(String name, String unit) {
        this.name = name;
        this.unit = unit;
    }
}
