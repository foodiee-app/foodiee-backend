package de.foodiee.foodieebackend.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.Getter;
import lombok.Setter;

@Embeddable
@Getter
@Setter
public class RecipeIngredientId implements Serializable {

    @Column(name = "recipe_id")
    private long recipeId;
    @Column(name = "ingredient_id")
    private long ingredientId;
}
