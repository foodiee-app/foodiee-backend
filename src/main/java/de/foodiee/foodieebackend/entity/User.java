package de.foodiee.foodieebackend.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;

@Entity
@Table(name = "users")
public class User {

    @Id
    @Getter
    @Column(name = "user_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Getter
    private String name;
    @Getter
    private String picturePath;

    protected User() {
    }

    public User(String name, String picturePath) {
        this.name = name;
        this.picturePath = picturePath;
    }
}
