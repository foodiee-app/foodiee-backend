package de.foodiee.foodieebackend.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import de.foodiee.foodieebackend.controller.requestbody.RecipeRequestBody;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Table(name = "recipe")
public class Recipe {

    @Id
    @Column(name = "recipe_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private long userId;
    private String name;
    @Setter
    private String thumbnailPath;
    private boolean isVeggie;
    private int preparationTime;
    @OneToMany(mappedBy = "recipe", cascade = CascadeType.ALL)
    private final Set<RecipeIngredient> recipeIngredients = new HashSet<>();
    @Setter
    private String preparation;

    protected Recipe() {
    }

    public Recipe(RecipeRequestBody requestBody) {

        this.userId = requestBody.getUserId();
        this.name = requestBody.getName();
        this.thumbnailPath = requestBody.getThumbnailPath();
        this.isVeggie = requestBody.isVeggie();
        this.preparationTime = requestBody.getPreparationTime();
        this.preparation = requestBody.getPreparation();
    }


    public void addIngredient(Ingredient ingredient, int amount) {
        recipeIngredients.add(new RecipeIngredient(this, ingredient, amount));
    }
}
