package de.foodiee.foodieebackend.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "shopping_list")
@Getter
public class ShoppingListEntry {
    @Id
    @Column(name = "ingredient_id")
    private long ingredientId;
    @Setter
    private int totalAmount;
    @OneToOne
    @JoinColumn(name = "ingredient_id")
    private Ingredient ingredient;

    protected ShoppingListEntry() {
    }

    public ShoppingListEntry(long ingredientId, int totalAmount) {
        this.ingredientId = ingredientId;
        this.totalAmount = totalAmount;
    }
}
