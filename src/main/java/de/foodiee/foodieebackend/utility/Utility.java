package de.foodiee.foodieebackend.utility;

public class Utility {

    private Utility() {
    }


    public static boolean isLong(String input) {
        try {
            Long.parseLong(input);
            return true;
        } catch (Exception exception) {
            return false;
        }
    }

    public static String getFileType(String fileName) {
        String[] array = fileName.split("\\.");
        return array[array.length - 1];
    }
}
